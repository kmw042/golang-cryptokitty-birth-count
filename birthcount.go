//go kmw042@gmail.com

package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"kc"
	"log"
	"math/big"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
)

type config struct {
	Net        string
	APIKey     string
	CkAPIToken string
}

// for production these would be in a config file
var cfg = config{
	Net:        "https://mainnet.infura.io/",
	APIKey:     "0x06012c8cf97BEaD5deAe237070F9587f8E7A266d",
	CkAPIToken: "71O8SyY9Axv9vomYWu9SLh_kEmiBnGGGZ5TEQ9imqhU",
}

// one kitty gene
type gr struct {
	Number   int    `json:"number"`
	Type     string `json:"type"`
	Gene     string `json:"gene"`
	Dominant string `json:"dominant"`
	Trait    string `json:"trait"`
}

// matron detail
type md struct {
	Name        string   `json:"name"`
	Birthdate   string   `json:"birthDateTime"`
	Generation  *big.Int `json:"generation"`
	Genes       string   `json:"genes"`
	GeneRecords []gr     `json:"geneRecords"`
}

// birth counts by matronID + matron details
type bcm struct {
	MatronID     *big.Int `json:"matronID"`
	Count        uint64   `json:"numberOfBirths"`
	MatronDetail *md      `json:"matronDetail"`
}

//return value
type bc struct {
	StartBlock uint64 `json:"startBlock"`
	EndBlock   uint64 `json:"endBlock"`
	Births     int    `json:"totalBirths"`
	TopMatrons *[]bcm `json:"matronsWithMostBirths"`
}

func getBirthCount(s uint64, e uint64) *bc {

	c := getEthContract(cfg.Net, cfg.APIKey)
	bes := getBirthEvents(c, s, e)
	res, t := buildResults(bes)
	eRes := evaluateResults(res)
	fRes := addMatronDetails(c, eRes)
	r := &bc{
		StartBlock: s,
		EndBlock:   e,
		Births:     t,
		TopMatrons: fRes}

	return r
}

func getBirthEvents(c *kc.Kc, s uint64, e uint64) <-chan *kc.KcBirth {

	var eb uint64

	//batch size for contract calls
	bs := uint64(2000)
	sb := s
	out := make(chan *kc.KcBirth)

	println("working...")

	go func() {
		for sb <= e {
			if (sb + bs) > e {
				eb = e
			} else {
				eb = sb + bs
			}
			fopts := bind.FilterOpts{Start: sb, End: &eb, Context: nil}
			bes, err := c.FilterBirth(&fopts)
			chkErr(err)

			println("working...")

			for bes.Next() {
				out <- bes.Event
			}
			sb += (bs + 1)
		}

		println("working...")

		close(out)
	}()
	return out
}

func buildResults(in <-chan *kc.KcBirth) (*map[uint64]uint64, int) {

	var fnd bool

	r := make(map[uint64]uint64)
	rt := 0

	for kb := range in {
		fnd = false
		mID := kb.MatronId.Uint64()
		if mID != 0 {
			rt++
			if len(r) > 0 {
				_, exists := r[mID]
				if exists {
					count := r[mID]
					r[mID] = count + 1
					fnd = true
				}
			}
			if fnd == false {
				r[mID] = 1
			}
		}
	}
	return &r, rt
}

func evaluateResults(mm *map[uint64]uint64) *[]bcm {

	max := uint64(0)
	r := make([]bcm, 0)

	for id, cc := range *mm {
		if cc > max {
			max = cc
			r = make([]bcm, 0)
		}
		if cc == max {
			nbcm := bcm{
				Count:    cc,
				MatronID: big.NewInt(int64(id)),
			}
			r = append(r, nbcm)
		}
	}
	return &r
}

func addMatronDetails(c *kc.Kc, bcms *[]bcm) *[]bcm {
	for i, minfo := range *bcms {
		momID := minfo.MatronID

		opts := bind.CallOpts(bind.CallOpts{})
		res, err := c.GetKitty(&opts, momID)
		chkErr(err)

		unixTime := time.Unix(res.BirthTime.Int64(), 0)
		unitTimeRFC3339 := unixTime.Format("Mon Jan _2 15:04:05 2006")

		//format gene string
		gene := fmt.Sprintf("%b", res.Genes)
		//leading 0s get stripped, add them back
		rl := 240 - len(gene)
		for j := 0; j < rl; j++ {
			gene = "0" + gene
		}

		//get matron name
		type kn struct {
			Name string `json:"name"`
		}
		m := kn{}
		mn := callCKApi("https://public.api.cryptokitties.co/v1/kitties/" +
			strconv.FormatInt(momID.Int64(), 10))
		json.Unmarshal([]byte(mn), &m)

		// add matron details to matron's birth count record
		mival := &md{
			Name:        m.Name,
			Birthdate:   unitTimeRFC3339,
			Generation:  res.Generation,
			Genes:       gene,
			GeneRecords: loadGenes(gene)}

		(*bcms)[i].MatronDetail = mival
	}
	return bcms
}

func loadGenes(gStr string) []gr {

	var gene string
	var gra []gr

	// there's no way to get this list in order by gene number from the ck api
	ts := [12]string{"UKNOWN", "SECRET", "environment", "mouth", "wild",
		"colortertiary", "colorsecondary", "colorprimary", "eyes", "coloreyes",
		"pattern", "body"}
	l := 5 //the length of a gene
	gn := 0
	grec := gr{}

	for i := 0; i < len(gStr); i += 5 {
		//one gene
		if l <= len(gStr) {
			gene = string(gStr[i:l])
			grec.Gene = grec.Gene + " " + gene
			l = l + 5
		}
		//four genes make up a trait
		if (i+1)%4 == 0 {
			grec.Number = gn
			grec.Type = (ts[gn])
			grec.Dominant = gene

			//get type ID
			tID, err := strconv.ParseInt(gene, 2, 64)
			chkErr(err)

			grec.Trait = getCattribute(strings.TrimSpace(grec.Type), strconv.FormatInt((tID), 10))
			gra = append(gra, grec)
			grec = gr{}
		}
		gn = i / 20
	}
	return gra
}

func getCattribute(trait string, num string) string {

	type ctb struct {
		Gene        int    `json:"gene"`
		Type        string `json:"type"`
		Description string `json:"description"`
	}

	ctbRec := ctb{}

	body := callCKApi("https://public.api.cryptokitties.co/v1/cattributes/" + trait + "/" + num)
	json.Unmarshal([]byte(body), &ctbRec)
	if len(ctbRec.Description) == 0 {
		ctbRec.Description = "none"
	}
	return ctbRec.Description
}

func stringifyResult(res *bc) string {
	out := fmt.Sprintln("Results for blocks " + strconv.FormatUint(res.StartBlock, 10) + " thru " + strconv.FormatUint(res.EndBlock, 10) + ":")
	out = out + fmt.Sprintln("Total Births:", strconv.FormatInt(int64(res.Births), 10))
	out = out + fmt.Sprintln("The maximum number of births for a single matron was", (*res.TopMatrons)[0].Count)
	out = out + fmt.Sprintln("Matron(s) that had", (*res.TopMatrons)[0].Count, "kittens:")
	for _, rec := range *(res).TopMatrons {
		out = out + fmt.Sprintln("\n"+rec.MatronDetail.Name)
		out = out + fmt.Sprintln("MatronID", strings.TrimSpace(strconv.FormatInt(rec.MatronID.Int64(), 10)))
		out = out + fmt.Sprintln("Born", strings.TrimSpace(rec.MatronDetail.Birthdate))
		out = out + fmt.Sprintln("Generation", strings.TrimSpace(strconv.FormatInt(rec.MatronDetail.Generation.Int64(), 10)))
		out = out + fmt.Sprintln("Complete Genome ", rec.MatronDetail.Genes)
		out = out + fmt.Sprintln()
		out = out + fmt.Sprintln(printGeneRecords(rec.MatronDetail.GeneRecords))
	}
	return out
}

func printGeneRecords(rec []gr) string {

	out := ""

	for row := 0; row < 6; row++ {
		pos := row * 2
		col := 2 * (row + 1)
		for i := pos; i < col; i++ {
			out = out + fmt.Sprintf("| %-35v|\t", "Gene Number: "+
				strconv.Itoa(rec[i].Number+1))
		}
		out = out + "\n"
		for i := pos; i < col; i++ {
			out = out + fmt.Sprintf("| %-35v|\t", "Type: "+(rec[i].Type))
		}
		out = out + "\n"
		for i := pos; i < col; i++ {
			out = out + fmt.Sprintf("| %-35v|\t", "Genes:"+(rec[i].Gene))
		}
		out = out + "\n"
		for i := pos; i < col; i++ {
			out = out + fmt.Sprintf("| %-35v|\t", "Dominant Gene: "+
				(rec[i].Dominant))
		}
		out = out + "\n"
		for i := pos; i < col; i++ {
			out = out + fmt.Sprintf("| %-35v|\t", "Trait: "+(rec[i].Trait))
		}
		out = out + "\n"
		out = out + "\n"
	}
	return out
}

func getEthContract(net string, addr string) *kc.Kc {
	client, err := ethclient.Dial(net)
	chkErr(err)

	instance, err := kc.NewKc(common.HexToAddress(addr), client)
	chkErr(err)

	return instance
}

func callCKApi(url string) []byte {
	req, err := http.NewRequest("GET", url, nil)
	chkErr(err)

	req.Header.Add("x-api-token", cfg.CkAPIToken)
	req.Header.Add("cache-control", "no-cache")
	res, err := http.DefaultClient.Do(req)
	chkErr(err)
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	chkErr(err)

	return body
}

func getUserInput() (uint64, uint64) {

	var bdiff int
	var starting uint64
	var ending uint64
	var err error

	scanner := bufio.NewScanner(os.Stdin)
	for bdiff < 1 {
		print("Enter starting block: ")
		scanner.Scan()
		if err = scanner.Err(); err != nil {
			log.Println(err)
		}
		starting, err = strconv.ParseUint(scanner.Text(), 10, 64)
		chkErr(err)
		print("Enter ending block: ")
		scanner.Scan()
		if err = scanner.Err(); err != nil {
			log.Println(err)
		}
		ending, err = strconv.ParseUint(scanner.Text(), 10, 64)
		chkErr(err)
		bdiff = int(ending - starting)
		if bdiff < 1 {
			println("Ending block is not greater than starting block")
			println("Please try again")
		}
	}
	return starting, ending
}

func setUpLog() *bufio.Writer {
	f, err := os.Create("./error.log")
	if err != nil {
		println("Unable to create log file, logging to console")
	}
	w := bufio.NewWriter(f)
	log.SetOutput(w)
	log.SetPrefix("birthcount.go : ")

	return w
}

func trace() string {
	pc := make([]uintptr, 15)
	n := runtime.Callers(4, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()

	return fmt.Sprintf("%s,:%d %s\n", frame.File, frame.Line, frame.Function,
		"\n")
}

func chkErr(err error) {
	if err != nil {
		panic(trace() + fmt.Sprintf("%v", err))
	}
}

func main() {

	w := setUpLog()

	//always exit gracefully
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Unrecoverable error, exiting program.  See log for details")
			log.Println(r)
			w.Flush()
			os.Exit(1)
		}
	}()

	starting, ending := getUserInput()
	ts := time.Now()
	result := getBirthCount(starting, ending)
	tf := time.Now()
	print(stringifyResult(result))
	fmt.Println("Execution time: ", tf.Sub(ts))
}
