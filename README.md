# CryptoKitty Birth Count

A simple command-line app to answer the question:

Given a startingBlock and endingBlock as arguments, count 
the total number of births that happened during that range. 
Finally, use that information to find the Kitty (birth timestamp, 
generation and their genes) that gave birth to the most kitties.

Source available on request
